var fs = require('fs'),
  express = require('express'),
  http = require('http'),
  app = express(),
  ldap = require('ldapjs'),
  ActiveDirectory = require('activedirectory'),
  bodyParser = require('body-parser'),
  cookieParser = require('cookie-parser'),
  mongo = require('mongodb'),
  mongodb = mongo.MongoClient,
  assert = require('assert'),
  mongoUrl = 'mongodb://10.131.146.239:27017/TestData',
  adGroupName = 'tavant team',
  adConfig = {
    url: 'ldap://10.129.135.230:3268/',
    baseDN: 'dc=corp,dc=tavant,dc=com',
    username: 'goutham.vinith@tavant.com',
    password: 'Secret@1219'
  },
  ad = new ActiveDirectory(adConfig),
  server, httpsConfig, database;

console.log('Starting App..');

/*======Create Server====*/
server = http.createServer(app).on('error', function(err) {
  console.log(err);
});

mongodb.connect(mongoUrl, function(err, db) {
  assert.equal(null, err);
  console.log('Successfully Connected to MongoDB..');
  database = db;
});

/*==========Verify Session on Requests=======*/
function checkSession(request) {
  var cookies = request.cookies.mapsSession;

  return (cookies ? true : false);
}

function errorHandler(response, error, message) {
  console.log(message);
  console.log(error);
  response.status(500).send();
}

app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/webroots', express.static(__dirname + '/webroots'));

app.use(function(request, response, next) {
  var isSessionActive;

  if ((request.path === '/') || (request.path === '/userValidation') || (request.path === '/favicon.ico')) {
    next();
  }
  else {
    isSessionActive = checkSession(request);
    if (isSessionActive) {
      next();
    }
    else {
      response.sendFile(__dirname + '/app_templates/login.html');
    }
  }
});

app.get('/', function(request, response) {
  var isSessionActive = checkSession(request);

  if (isSessionActive) {
    response.sendFile(__dirname + '/app_templates/index.html');
  }
  else {
    response.sendFile(__dirname + '/app_templates/login.html');
  }
});

app.get('/admin', function(request, response){
  var userEmail = request.cookies.mapsSession;

  database.collection('users').findOne({email : userEmail}, {_id: 0}, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in ckecking Admin User..');
    }
    else if(data){
      console.log('Admin login : ' + data.userName);
      response.sendFile(__dirname + '/app_templates/admin.html');
    }
    else{
      console.log('Admin access rejected : ' + userEmail);
      response.sendFile(__dirname + '/app_templates/index.html');
    }
  });
});

app.post('/userValidation', function(request, response) {
  var client, opts,
    userName = request.body.username + '@tavant.com',
    password = request.body.password;

  client = ldap.createClient({
    url: 'ldap://10.129.135.230:3268/',
    timeout: 5000,
    connectTimeout: 10000
  });

  console.log('Connecting User ' + userName + '..');

  try {
    client.bind(userName, password, function(error) {
      if (error) {
        console.log(error.message);
        client.unbind(function(error) {
          if (error) {
            console.log(error.message);
          }
          else {
            //console.log('Client disconnected');
          }
        });
        response.status(200).send(false);
      }
      else {
        console.log('Successfully Connected User ' + userName + ' at ' + new Date() + '..');

        response.cookie('mapsSession', userName);
        response.status(200).send(true);
      }
    });
  }
  catch (error) {
    console.log(error);
    client.unbind(function(error) {
      if (error) {
        console.log(error.message);
      }
      else {
        console.log('Client disconnected');
      }
    });
    response.status(200).send(false);
  }

});

/*=======Get LoggedIn User Role======*/
app.get('/getUserRole', function(request, response){
  var userEmail = request.cookies.mapsSession;

  database.collection('users').findOne({email : userEmail}, {_id: 0, email: 1, role: 1}, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in fetching current Internal User role..');
    }
    else {
      response.send(data.role);
    }
  });
});

/*=======Get All Internal Users======*/
app.get('/getAllUsers', function(request, response){
  database.collection('users').find().toArray(function(error, data){
    if(error){
      errorHandler(response, error, 'Error in fetching all Internal Users..');
    }
    else {
      response.send(data);
    }
  });
});

/*=====Get All ActiveDirectory Users======*/
app.get('/getADUsers', function (request, response) {
  ad.getUsersForGroup(adGroupName, function (err, users) {
    var i, len, currentUser, adUsers = [];
    if (err) {
      console.log('ERROR: ' + JSON.stringify(err));
      response.status(500).send(err);
    }

    if (!users) {
      console.log('Group: ' + adGroupName + ' not found.');
      response.send([]);
    }

    len = users.length;
    for (i = 0; i < len; i++){
      currentUser = users[i];

      adUsers.push({
        name: currentUser.cn,
        email: currentUser.mail
      });

      currentUser = null;
    }

    response.send(adUsers);
  });
});

/*=========Save New User==========*/
app.post('/saveUser', function(request, response){
  var userData = request.body;

  database.collection('users').insert(userData, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in saving new user..');
    }
    else{
      response.send();
    }
  });
});

/*=======Update User Details======*/
app.put('/saveUser', function(request, response){
  var userData = request.body,
    mongoId = new mongo.ObjectID(userData._id);

  userData._id = mongoId;

  database.collection('users').update({_id : mongoId}, userData, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in updating user..');
    }
    else{
      response.send();
    }
  });
});

/*=======Update User Details======*/
app.post('/deleteUser', function(request, response){
  var userData = request.body,
    mongoId = new mongo.ObjectID(userData._id);

  database.collection('users').remove({_id : mongoId}, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in deleting user..');
    }
    else{
      response.send();
    }
  });
});

/*=======Get All Clients and Active Status======*/
app.get('/getClients', function(request, response){
  var requiredFields = {clientName: 1, isClientActive: 1};

  database.collection('clients').find({}, requiredFields).toArray(function(error, data){
    if(error){
      errorHandler(response, error, 'Error in fetching all clients..');
    }
    else{
      response.send(data);
    }
  });
});

/*=======Get All Practices======*/
app.get('/getPractices', function(request, response){
  database.collection('practices').find({}, { _id: 0 }).sort({ 'practice': -1, 'field': 1 }).toArray(function(error, data){
    if(error){
      errorHandler(response, error, 'Error in fetching all Practices..');
    }
    else{
      response.send(data);
    }
  });
});

/*===Get Details Of Client and Related Projects===*/
app.post('/getClient', function(request, response){
  var mongoId = new mongo.ObjectID(request.body._id);

  database.collection('clients').findOne({_id : mongoId}, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in fetching details of a client..');
    }
    else{
      response.send(data);
    }
  });
});

/*=========Save New Client==========*/
app.post('/saveClient', function(request, response){
  var clientData = request.body,
    isExternal = clientData.isExternal;

  delete clientData['isExternal'];

  database.collection('clients').insert(clientData, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in saving new client..');
    }
    else {
      isExternal && console.log('Client created successfully by Emp Connect at ' + new Date());
      response.send((data.ops && data.ops[0]) || clientData);
    }
  });
});

/*=======Update Client Details or Status======*/
app.put('/saveClient', function(request, response){
  var clientData = request.body,
    isExternal = clientData.isExternal,
    mongoId = new mongo.ObjectID(clientData._id),
    queryData = clientData.isClientStatusChanged ? { $set: { isClientActive: clientData.isClientActive } } : clientData,
    updateCondition = isExternal ? { _clinetId: clientData._clinetId } : { _id: mongoId };

  delete clientData['_id'];
  delete clientData['isClientStatusChanged'];
  delete clientData['isExternal'];

  database.collection('clients').update(updateCondition, queryData, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in updating client..');
    }
    else {
      isExternal && console.log('Client updated successfully by Emp Connect at ' + new Date());
      response.send(clientData);
    }
  });
});

/*====Get All Projects of a Client====*/
app.post('/getProjects', function(request, response){
  var requiredFields = {clientId: 1, projectName: 1, isProjectActive: 1};

  database.collection('projects').find({clientId: request.body._id}, requiredFields).toArray(function(error, data){
    if(error){
      errorHandler(response, error, 'Error in fetching all projects of client..');
    }
    else{
      response.send(data);
    }
  });
});

/*===Get Details Of Project===*/
app.post('/getProject', function(request, response){
  var mongoId = new mongo.ObjectID(request.body._id);

  database.collection('projects').findOne({_id : mongoId}, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in fetching details of project..');
    }
    else{
      response.send(data);
    }
  });
});

/*=========Save New Project==========*/
app.post('/saveProject', function(request, response){
  var projectData = request.body,
    isExternal = projectData.isExternal;

  delete projectData['isExternal'];

  database.collection('projects').insert(projectData, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in saving new project of client..');
    }
    else {
      isExternal && console.log('Project created successfully by Emp Connect at ' + new Date());
      response.send((data.ops && data.ops[0]) || projectData);
    }
  });
});

/*=======Update Project Details or Status======*/
app.put('/saveProject', function(request, response){
  var projectData = request.body,
    isExternal = projectData.isExternal,
    mongoId = new mongo.ObjectID(projectData._id),
    queryData = projectData.isProjectStatusChanged ? { $set: { isProjectActive: projectData.isProjectActive } } : projectData,
    updateCondition = projectData.isExternal ? { _projectId: projectData._projectId } : { _id: mongoId };

  delete projectData['_id'];
  delete projectData['isProjectStatusChanged'];
  delete projectData['isExternal'];

  database.collection('projects').update(updateCondition, queryData, function(error, data){
    if(error){
      errorHandler(response, error, 'Error in updating existing project of client..');
    }
    else {
      isExternal && console.log('Project updated successfully by Emp Connect at ' + new Date());
      response.send(projectData);
    }
  });
});

/*=======Get All Clients for Maps======*/
app.get('/getClientsList', function(request, response){
  database.collection('clients').find().toArray(function(error, data){
    if(error){
      errorHandler(response, error, 'Error in fetching all projects of client..');
    }
    else{
      response.send(data);
    }
  });
});

/*=======Get All Projects for Maps======*/
app.get('/getProjectsList', function(request, response){
  database.collection('projects').find().toArray(function(error, data){
    if(error){
      errorHandler(response, error, 'Error in fetching all projects of client..');
    }
    else{
      response.send(data);
    }
  });
});

app.get('/logout', function(request, response) {
  var user = request.cookies.mapsSession;

  console.log('Successfully Logged out user ' + user + ' at ' + new Date() + '..');
  response.clearCookie('mapsSession');
  response.status(200).send(true);
});

process.on('uncaughtException', function (err) {
  console.error(err);
  console.log("Node continues to run...");
});

server.listen(8000);
console.log('Started Application on port 8000..');