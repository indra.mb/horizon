$(document).ready(function() {

  'use strict';
  /*=========Set Variables=========*/
  var selectedList = {},
    subProjectList = {},
    tempClientsList, tempProjectsList,
    currentClient = {},
    modifiedPractices = {},
    projects, fieldLabel, treeHead, quickSearch = {},
    searchSuggestions, searchText = '',
    filteredProjectInformation = [],
    markers = [],
    practicesNames = [],
    checkboxes, selectedTags = [],
    isSubProject = false,
    subProjectClass = '',
    isClientShown = false,
    isDescription = false,
    clientInformation, tavantList = [],
    filteredClients = [],
    filteredProjects = [],
    descriptionClientInfo = [],
    descriptionFilteredInfo = [],
    searchCriteriaArray = ['technology', 'db', 'os'],
    descriptionCriteriaArray = ['projectDescription', 'description'],
    markerImagesPath = 'webroots/images/',
    markerIconsName = {
      'Capital Market': 'capitalmarket_map.png',
      'Manufacturing': 'manufacturing_map.png',
      'Media & Entertainment': 'media_map.png',
      'eBusiness': 'eBusiness_map.png',
      'IST': 'QA_map.png',
      'BI': 'BI_map.png',
      'Mobile': 'mobile_map.png',
      'DevOps': 'do_map.png',
      'Digital': 'digital_map.png',
      'Salesforce': 'sf_map.png',
      'Inactive': 'inactive_map.png',
      'DIT': 'dit_map.png',
      'FinTech': 'FinTech_map.png',
      'Analytics':'analytics_map.png',
      'App':'app_map.png',
      'DevOps':'DevOps_map.png',
      'Retail':'retail_map.png',
      'Tavant':'tavant_map.png',
      'UI':'UI_map.png',
    },
    fieldNameList = Object.keys(markerIconsName),
    fieldNameListLen = fieldNameList.length,
    descriptionText = '',
    urlPrefix = location.protocol + '//' + location.hostname + ':' + location.port,
    fieldSection;

  /*=========Map Variables========*/
  var map = null,
    infowindow = null;

  /*=========Dom Variables=========*/
  var clientsDropDown = $('#clients'),
    searchField = $('#searchField'),
    clientDetails = $('.clientDetails'),
    searchResults = $('.searchResults'),
    descriptionSearch = $('#descriptionSearch'),
    paneToggleBtn = $('.pane-toggle-Btn'),
    layerDiv = $('.layerDiv'),
    searchFieldContainer = $('.searchFieldContainer'),
    tagsWrapper = $('.tagsWrapper'),
    clientProjects = $('.clientProjects'),
    clientContent = $('.clientContent'),
    scrollDiv = $('.scrollDiv'),
    clientDetailsWrapper = $('.clientDetailsWrapper');

  /*========Initialize DOM Elements=======*/
  searchResults.hide();
  clientDetails.hide();
  clientProjects.hide();

  /*=========Data Service Call===========*/

  //Get All Clients
  $.get(urlPrefix + '/getClientsList', function(data) {
    clientInformation = splitDataComma(data, true);
  });

  //Get All Projects
  $.get(urlPrefix + '/getProjectsList', function(data) {
    projects = splitDataComma(data);
  });

  //Get All Practices
  $.get(urlPrefix + '/getPractices', function(data) {
    practicesManipulation(data);
    initializePracticesContent();
  });

  /*=======All DOM Elements Events========*/
  paneToggleBtn.click(function() {
    if (!layerDiv.hasClass('layerOffScreen')) {
      if (!clientProjects.hasClass('offScreenProjects')) {
        clientProjects.addClass('offScreenProjects');
        setTimeout(function() {
          layerDiv.toggleClass('layerOffScreen');
          clientProjects.hide();
        }, 400);
      }
      else {
        layerDiv.toggleClass('layerOffScreen');
        clientProjects.hide();
      }
    }
    else {
      layerDiv.toggleClass('layerOffScreen');
      if (isClientShown) {
        clientProjects.show();
      }
    }
  });

  $('.clientProjectsBtn').click(function() {
    clientProjects.toggleClass('offScreenProjects');
  });

  /*========Search Box Focus and Blur Events=======*/
  searchField.focus(function() {
    if (searchField.val().length > 0) {
      createSearchSuggestion();
      searchResults.show();
    }
  });

  searchField.blur(function(evt) {
    var activeElement;

    setTimeout(function() {
      activeElement = document.activeElement;
      if ($(activeElement).closest(searchResults).length === 0) {
        searchResults.hide();
      }
    });
  });

  /*=====Convert Comma Separated Values to Arrays===*/
  function splitDataComma(data, isClientsData) {
    var splitData, keys, currentKey, len, i, itemLen, j, dataLen, k, currentObj;

    dataLen = data.length;
    for (k = 0; k < dataLen; k++) {
      currentObj = data[k];
      keys = Object.keys(currentObj);
      len = keys.length;
      for (i = 0; i < len; i++) {
        currentKey = keys[i];
        if ((currentKey !== 'description') && (currentKey !== 'projectDescription')) {
          if (typeof(currentObj[currentKey]) === 'string') {
            splitData = currentObj[currentKey].split(',');
            itemLen = splitData.length;
            if (itemLen > 1) {
              for (j = 0; j < itemLen; j++) {
                splitData[j] = $.trim(splitData[j]);
              }
              currentObj[currentKey] = splitData;
            }
          }
          currentKey = null;
        }
      }
      data[k] = currentObj;
      if(isClientsData){
        data[k].projects = [];
      }
      currentObj = null;
    }
    return data;
  }

  /*===Manipulate the List of Horizontal and Vertical Practices===*/
  function practicesManipulation(practices) {
    var tempPractice = {},
      tempField = {},
      i, len;

    for (i = 0; i < practices.length; i++) {
      modifiedPractices[practices[i].practice] = modifiedPractices[practices[i].practice] || [];
      modifiedPractices[practices[i].practice].push(practices[i].field);
    }
    $.each(modifiedPractices, function(key, value) {
      tempPractice.depts = [];
      len = value.length;
      tempPractice.field = key;
      for (i = 0; i < len; i++) {
        tempField.field = value[i];
        tempPractice.depts.push(tempField);
        tempField = {};
      }
      tavantList.push(tempPractice);
      tempPractice = {};
    });
    practicesNames = modifiedPractices.Horizontal.reverse().concat(modifiedPractices.Vertical.reverse());
  }

  /*========Creating Practices Segment Dynamically=====*/
  function initializePracticesContent() {
    createPracticesContent(tavantList, clientsDropDown);
    fieldLabel = clientsDropDown.children('.select-box-list').find('label');
    treeHead = clientsDropDown.find('li.treeHead');
    fieldSection = treeHead.find('.select-box-list').hide();
    fieldLabel.off('click').on('click', labelClick);
    checkboxes = clientsDropDown.find('span.checkBox');
  }

  function createPracticesContent(data, parentElement) {
    var i, len, currentData;

    parentElement.append('<ul class="select-box-list"></ul>');
    parentElement = $(parentElement).children('ul.select-box-list').last();
    len = data.length;
    for (i = 0; i < len; i++) {
      currentData = data[i];
      if (currentData.depts) {
        subProjectClass = (currentData.field === 'Horizontal') ? 'subProject' : '';
        parentElement.append('<li> <label class="' + subProjectClass + '"> <span class="practiceToggle animate"></span> <span class="checkBox"> </span> ' + currentData.field + 
          ' </label> </li>');
        createPracticesContent(currentData.depts, parentElement.children('li').last().addClass('treeHead'));
      }
      else {
        parentElement.append('<li> <span class="fieldSymbol ' + currentData.field.split(' ')[0] + '"> </span> <label class="' + subProjectClass + '"> <span class="checkBox"> </span> ' + 
          currentData.field + ' </label> </li>');
      }
      currentData = null;
    }

    $('.practiceToggle').off('click').on('click', function(event) {
      $(this).parent().next().slideToggle(200);
      $(this).toggleClass('open');
      event.stopPropagation();
    });
  }

  /*===========Practices-Segment Click Functionality=========*/
  function labelClick($evt) {
    var name, labels, i, len, keys,
      labelElement = $(this),  
      isChecked = labelElement.children('span.checkBox').toggleClass('checked').hasClass('checked'),
      isHead = labelElement.parent().hasClass('treeHead');

    hideClientDetails();
    isClientShown = false;
    filteredClients = [];
    filteredProjects = [];

    /* If the Clicked label is Sub-Project */
    if (!isHead) {
      name = $.trim(labelElement.text());
      isSubProject = labelElement.hasClass('subProject') ? true : false;

      modifyCriteria();
      applyCriteria();
      headerLabelCheck();
    }
    /* If the Clicked label is Heading */
    else {
      labels = labelElement.siblings('.select-box-list').find('label');
      isSubProject = labelElement.hasClass('subProject') ? true : false;
      len = labels.length;

      /* Toggle Custom Checkbox */
      if (isChecked) {
        labels.find('span.checkBox').addClass('checked');
      }
      else {
        labels.find('span.checkBox').removeClass('checked');
      }

      for (i = 0; i < len; i++) {
        name = $.trim($(labels[i]).text());
        modifyCriteria();
      }

      applyCriteria();

      if (name === currentClient.practice) {
        hideClientDetails();
      }
    }

    /* Modify the Selected Horizontal/Veritical Project List */
    function modifyCriteria() {
      if (isChecked && isSubProject) {
        subProjectList[name] = true;
      }
      else if (isSubProject) {
        delete subProjectList[name];
      }
      else if (isChecked) {
        selectedList[name] = true;
      }
      else {
        delete selectedList[name];
      }
    }

    function applyCriteria() {
      /* Filter Projects based on Horizontal Practices */
      tempProjectsList = $.extend([], projects);
      keys = Object.keys(subProjectList);
      len = keys.length;
      for (i = 0; i < len; i++) {
        filteredProjects = filteredProjects.concat(filterClientData(['horizontal'], keys[i], tempProjectsList));
      }

      /* Filter Clients based on Vertical Practices */
      tempClientsList = $.extend([], clientInformation);
      keys = Object.keys(selectedList);
      len = keys.length;
      for (i = 0; i < len; i++) {
        filteredClients = filteredClients.concat(filterClientData(['field'], keys[i], tempClientsList));
      }
    }

    mapDataManipulation();
  }

  /* To Check if Head Check Box should be checked or not on click of Practice Label */
  function headerLabelCheck() {
    var i, treeHeadBoxes;

    for (i = 0; i < treeHead.length; i++) {
      treeHeadBoxes = $(treeHead[i]).find('span.checkBox');
      $(treeHeadBoxes[0]).removeClass('checked');
      if ((treeHeadBoxes.length - 1) === $(treeHead[i]).find('span.checkBox.checked').length) {
        $(treeHeadBoxes[0]).addClass('checked');
      }
      treeHeadBoxes = null;
    }
  }

  //Method to delay and hide Client Details based on Projects pane open state
  function hideClientDetails() {
    if (!clientProjects.hasClass('offScreenProjects')) {
      clientProjects.addClass('offScreenProjects');
      setTimeout(function() {
        hideDetails();
      }, 400);
    }
    else {
      hideDetails();
    }
  }

  function hideDetails() {
    $('.clientDetails:visible').slideToggle(300);
    clientProjects.hide();
    clientDetailsWrapper.addClass('noPad');
    currentClient = {};
  }

  /*=========Technology Search Box Functionalities=========*/
  searchField.off('keyup').on('keyup', function(evt) {
    var selectedResult;
    $(this).val($(this).val().replace(/[^a-zA-Z0-9 .&-]/g, '').replace(/\s+/g, ' '));
    searchText = $.trim($(this).val().toLowerCase());

    if (!(evt.keyCode) || (evt.keyCode === 13)) {
      searchResults.hide();
      if (!(evt.keyCode)) {
        appendTag();
      }
      else {
        selectedResult = searchResults.children().first();
        if (selectedResult.length > 0) {
          searchText = $.trim($(selectedResult).text().split(',')[0]);
          appendTag();
        }
      }

      if (!(layerDiv.hasClass('layerOffScreen'))) {
        paneToggleBtn.trigger('click');
      }
    }
    else if (evt.keyCode === 40) {
      searchSuggestions.first().focus();
      searchField.val(searchSuggestions.first().text().split(',')[0]);
    }
    else if (evt.keyCode === 38) {
      searchSuggestions.last().focus();
      searchField.val(searchSuggestions.last().text().split(',')[0]);
    }
    else if (searchText.length > 0) {
      createSearchSuggestion();
    }
    else {
      searchResults.hide();
      searchResults.html('');
      filterTag();
      if (selectedList.length === 0) {
        searchBoxEmpty();
      }
    }
  });

  /*=========Description Search Box Functionalities=========*/
  descriptionSearch.off('keyup').on('keyup', function(evt) {
    if (event.keyCode === 13) {
      $(this).val($(this).val().replace(/[^a-zA-Z0-9 .&-]/g, '').replace(/\s+/g, ' '));
      descriptionText = $.trim($(this).val().toLowerCase());
      descriptionFilteredInfo = [];
      descriptionClientInfo = [];
      if (descriptionText.length > 0) {
        hideClientDetails();
        tempProjectsList = $.extend([], projects);
        tempClientsList = $.extend([], clientInformation);
        isDescription = true;
        descriptionFilteredInfo = filterClientData(descriptionCriteriaArray, descriptionText, tempProjectsList);
        descriptionClientInfo = filterClientData(descriptionCriteriaArray, descriptionText, tempClientsList);
        isDescription = false;
      }
      mapDataManipulation();
    }
    else if ((event.keyCode === 8) && ($(this).val().length === 0)) {
      descriptionFilteredInfo = [];
      descriptionClientInfo = [];
      hideClientDetails();
      mapDataManipulation();
    }
  });

  /*========Add Search Tag Functionality=====*/
  function appendTag() {
    selectedTags.push(searchText);
    tagsWrapper.append('<label class="searchTag">' + searchText + '<span class="removeTag"> </span> </label>');
    $('.removeTag').off('click').on('click', clearTag);
    filterTag();
  }

  /*=========Cancel Tag Functionality=======*/
  function clearTag() {
    var name = $.trim($(this).parent().text()),
      index = selectedTags.indexOf(name);

    selectedTags.splice(index, 1);
    $(this).parent().remove();
    filterTag();
  }

  /*==========To Check if Tag is already Added======*/
  function filterTag() {
    var i, len;

    filteredProjectInformation = [];
    tempProjectsList = $.extend([], projects);
    len = selectedTags.length;
    if (len > 0) {
      for (i = 0; i < len; i++) {
        filteredProjectInformation = filteredProjectInformation.concat(filterClientData(searchCriteriaArray, selectedTags[i].toLowerCase(), tempProjectsList));
      }
    }
    mapDataManipulation();
    searchField.val('');
  }

  /*=====Functionality when Search Box is Empty======*/
  function searchBoxEmpty() {
    if (layerDiv.hasClass('layerOffScreen')) {
      paneToggleBtn.trigger('click');
    }
    searchFieldContainer.addClass('tooltip-parent');
    setTimeout(function() {
      searchFieldContainer.removeClass('tooltip-parent');
    }, 3000);
  }

  /*=========Creating Search Suggestions=========*/
  function createSearchSuggestion() {
    var name, i, j, reg, keys, len, tagsLen, index, limit;

    searchResults.show();
    quickSearch = {};
    tempProjectsList = $.extend([], projects);
    filterClientData(searchCriteriaArray, searchText, tempProjectsList, true);
    keys = Object.keys(quickSearch);
    len = keys.length;
    if (len > 0) {
      keys.sort();
      tagsLen = selectedTags.length;
      for (j = 0;
        (j < tagsLen) && (len > 0); j++) {
        if (quickSearch[selectedTags[j]]) {
          delete quickSearch[selectedTags[j]];
          index = keys.indexOf(selectedTags[j]);
          keys.splice(index, 1);
          len--;
        }
      }
    }
    if (len > 0) {
      searchResults.html('');
      limit = (len > 5) ? 5 : len;
      for (i = 0; i < limit; i++) {
        reg = new RegExp(searchText, 'gi');
        reg = keys[i].replace(reg, function(str) {
          return '<b>' + str + '</b>';
        });
        name = fieldNames(quickSearch[keys[i]]);
        searchResults.append('<a href="javascript:void(0)"><label>' + reg + '</label>, <span>' + name + '</span> </a>');
      }
      searchSuggestions = [];
      searchSuggestions = searchResults.find('a');
      searchSuggestions.on('keyup', navigateSuggestion);
      searchSuggestions.on('click', selectSuggestion);
    }
    else {
      searchResults.hide();
    }
  }

  /*=====To Set Focus on Search Box=====*/
  function setFocus() {
    searchField.focus();
    searchField.val(searchText);
  }

  /*======Search Suggestion Key Navigation Functionalities======*/
  function navigateSuggestion(evt) {
    switch (evt.keyCode) {
      case 40:
        if ($(this).next().length > 0) {
          $(this).next().focus();
          searchField.val($(this).next().text().split(',')[0]);
        }
        else {
          setFocus();
        }
        break;
      case 38:
        if ($(this).prev().length > 0) {
          $(this).prev().focus();
          searchField.val($(this).prev().text().split(',')[0]);
        }
        else {
          setFocus();
        }
        break;
      case 13:
        selectSuggestion();
        break;
      case 27:
        searchResults.hide();
        setFocus();
        break;
      default:
        break;
    }
  }

  /*=========Functionality on Selecting a Search Suggestion===========*/
  function selectSuggestion() {
    searchField.val($(this).text().split(',')[0]);
    searchField.trigger('keyup');
    searchResults.hide();
  }

  /*==========Displaying Client Details on Marker Click==========*/
  function showClientDetails(details) {
    var heading, i, clientContentHTML, clientDetailsHTML, clientSectionPane, description,
      activeCheck = '';

    if (layerDiv.hasClass('layerOffScreen')) {
      paneToggleBtn.trigger('click');
    }
    isClientShown = true;
    if (details.clientName !== currentClient.clientName) {
      heading = '<h1> <a target="_blank" href="' + details.link + '" class="tooltip-parent">' + details.clientName +
        ' <span class="tooltip tooltip-bottom"> Go to Client Website </span></a> </h1> <ul>';
      description = '<div class="description"> <h2> About ' + details.clientName + ' </h2> <p> ' + details.description + '</p></div>';
      clientDetailsHTML = heading;

      currentClient = {};
      currentClient = $.extend({}, details);
      clientProjects.hide();
      $('.clientDetails:visible').slideToggle(300);
      clientProjects.addClass('offScreenProjects');
      setTimeout(function() {
        $.each(currentClient, function(key, value) {
          if ((key === 'client') || (key === 'location') || (key === 'projectSince')) {
            clientDetailsHTML = clientDetailsHTML + '<li> <label>' + fieldNames(key) + '</label><span> ' + value + '</span><br> </li>';
          }
        });

        clientDetailsHTML = clientDetailsHTML + '</ul>' + description;
        clientDetails.html('');
        clientContent.html('');
        clientDetailsHTML = clientDetailsHTML.replace(/,/g, ', ');
        clientDetails.html(clientDetailsHTML);
        clientDetails.slideToggle(300);
        clientDetailsWrapper.addClass('noPad');

        for (i = 0; i < currentClient.projects.length; i++) {
          activeCheck = currentClient.projects[i].isProjectActive ? '' : ' (Inactive)';

          clientContentHTML = '<div class="clientPane"> <h2 title="' + currentClient.projects[i].projectName + activeCheck + ' " class="' +
            (currentClient.projects[i].isProjectActive ? '' : 'inactive') + '">' + currentClient.projects[i].projectName + activeCheck + ' <span></span></h2><div class="clientSection"> <ul>';

          $.each(currentClient.projects[i], function(key, value) {
            if ((key === 'technology') || (key === 'db') || (key === 'os')) {
              clientContentHTML = clientContentHTML + '<li> <label>' + fieldNames(key) + '</label><span> ' + value + '</span><br> </li>';
            }
            else if (key === 'projectLink') {
              clientContentHTML = clientContentHTML + '<li> <label>' + fieldNames(key) + '</label><span>' +
                '<a href="' + value + '" target="_blank"> ' + value + '</a></span><br> </li>';
            }
          });
          clientContentHTML = clientContentHTML + '</ul> <h3> Project Description : </h3> <p> ' +
            currentClient.projects[i].projectDescription + '</p> </div> </div>';
          clientContentHTML = clientContentHTML.replace(/,/g, ', ');
          clientContent.append(clientContentHTML);

          clientContentHTML = null;
        }
        clientSectionPane = clientContent.find('.clientSection');
        clientSectionPane.hide();
        clientSectionPane.first().show().parent().addClass('active');

        clientContent.find('h2').click(function() {
          $(this).parent().siblings().removeClass('active').find('.clientSection:visible').slideToggle(300);
          $(this).siblings('.clientSection:hidden').slideToggle(300).parent().addClass('active');
        });

        setTimeout(function() {
          $('.clientProjects:hidden').slideToggle(300);
          clientDetailsWrapper.removeClass('noPad');
        }, 200);

        if ($(window).height() < layerDiv.height()) {
          scrollDiv.addClass('overflowDiv');
        }
        else {
          scrollDiv.removeClass('overflowDiv');
        }

        clientSectionPane = null;
      }, 400);
    }
  }

  /*======Changing Object Key to Readable Field Name====*/
  function fieldNames(key) {
    var x = key.replace(/([A-Z])/g, " $1");
    return (x.charAt(0).toUpperCase() + x.slice(1));
  }

  /*=====Filter Clients Data=======*/
  function filterClientData(criteriaArray, filterVal, data, isSuggestion) {
    var i, filteredData = [],
      keys, keysLen, currentKey, lowerCaseVal, currentData, value, arrayLen, j, k,
      len = data.length;

    for (i = 0; i < len; i++) {
      currentData = data[i];
      keys = Object.keys(currentData);
      keysLen = keys.length;
      for (j = 0; j < keysLen; j++) {
        currentKey = keys[j];
        if (criteriaArray.indexOf(currentKey) > -1) {
          value = currentData[currentKey];
          if (isSuggestion) {
            if ($.isArray(value)) {
              arrayLen = value.length;
              for (k = 0; k < arrayLen; k++) {
                lowerCaseVal = value[k].toLowerCase();
                if (lowerCaseVal.indexOf(filterVal.toLowerCase()) > -1) {
                  if (!quickSearch[lowerCaseVal] || (quickSearch[lowerCaseVal] !== currentKey)) {
                    quickSearch[lowerCaseVal] = currentKey;
                  }
                }
                lowerCaseVal = null;
              }
            }
            else if (value && value.toLowerCase().indexOf(filterVal.toLowerCase()) > -1) {
              lowerCaseVal = value.toLowerCase();
              if (!quickSearch[lowerCaseVal] || (quickSearch[lowerCaseVal] !== currentKey)) {
                quickSearch[lowerCaseVal] = currentKey;
              }
              lowerCaseVal = null;
            }
          }
          else if ($.isArray(value)) {
            arrayLen = value.length;
            for (k = 0; k < arrayLen; k++) {
              if (value[k].toLowerCase() === filterVal.toLowerCase()) {
                filteredData.push(currentData);
                if (currentData._id) {
                  tempProjectsList.splice(i, 0);
                }
                else {
                  tempClientsList.splice(i, 0);
                }
                break;
              }
            }
          }
          else if ((isDescription && value.toLowerCase().indexOf(filterVal.toLowerCase()) > -1) ||
            (value && value.toLowerCase() === filterVal.toLowerCase())) {
            filteredData.push(currentData);
            if (currentData._id) {
              tempProjectsList.splice(i, 0);
            }
            else {
              tempClientsList.splice(i, 0);
            }
            break;
          }
        }
      }
    }

    return filteredData;
  }

  /*======To Combine Combination of Filters========*/
  function mapDataManipulation() {
    var i, j, keys, keysLen, finalList = [],
      projectList,
      selectedLen, tempClient, selectedProjects = [],
      len = filteredClients.length,
      descriptionLen = descriptionClientInfo.length,
      isPresent = false;

    $.extend(selectedProjects, descriptionFilteredInfo);
    $.extend(selectedProjects, filteredProjectInformation);
    selectedLen = selectedProjects.length;

    tempClientsList = len ? $.extend([], filteredClients) : $.extend([], clientInformation);
    keys = Object.keys(subProjectList);
    keysLen = keys.length;

    /* If Technology or Description Projects */
    if (selectedLen) {

      if (keysLen) {
        for (i = 0; i < selectedLen; i++) {
          for (j = 0; j < keysLen; j++) {
            if (selectedProjects[i].horizontal.indexOf(keys[j]) > -1) {
              isPresent = true;
              break;
            }
          }
          if (!isPresent) {
            selectedProjects.splice(i, 1);
            i--;selectedLen--;
          }
          isPresent = false;
        }

        for (i = 0; i < descriptionLen; i++) {
          for (j = 0; j < keysLen; j++) {
            if (descriptionClientInfo[i].field.indexOf(keys[j]) > -1) {
              isPresent = true;
              break;
            }
          }
          if (!isPresent) {
            descriptionClientInfo.splice(i, 1);
            i--;descriptionLen--;
          }
          isPresent = false;
        }
      }
      setFilterDescription();
    }
    /* If Description Clients */
    else if (descriptionLen) {
      setFilterDescription();
    }
    /* If Hotizontal Projects */
    else if (keysLen) {
      selectedProjects = $.extend([], filteredProjects);
      finalList = combineResults(finalList, selectedProjects, tempClientsList);
    }
    /* If no Additional Filter Criteria */
    else {
      if (len) {
        for (i = 0; i < len; i++) {
          projectList = $.grep(projects, function(project) {
            return project.clientId === filteredClients[i]._id;
          });

          filteredClients[i].projects = projectList;
          finalList.push(filteredClients[i]);
          projectList = null;
        }
      }
    }

    /* Filter Based on Description Clients */
    function filterDescription(finalClients) {
      var project, addedProjects = {},
        finalLen = finalClients.length,
        projectsLen = projects.length;

      selectedLen = selectedProjects.length;
      for (i = 0; i < selectedLen; i++) {
        addedProjects[selectedProjects[i]._id] = true;
      }

      for (i = 0; i < finalLen; i++) {
        tempClient = finalClients[i];

        for (j = 0; j < projectsLen; j++) {
          project = projects[j];
          if ((project.clientId === tempClient._id) && (!addedProjects[project._id])) {
            addedProjects[project._id] = true;
            selectedProjects.push(project);
          }
          project = null;
        }
      }
      /* Filter Only Description Clients Satisfying Horizontal Projects */
      if (keysLen) {
        selectedLen = selectedProjects.length;
        for (i = 0; i < selectedLen; i++) {
          for (j = 0; j < keysLen; j++) {
            if (selectedProjects[i].horizontal.indexOf(keys[j]) > -1) {
              isPresent = true;
              break;
            }
          }
          if (!isPresent) {
            selectedProjects.splice(i, 1);
            i--;
            selectedLen--;
          }
          isPresent = false;
        }
      }
    }

    /* Set Filter Based on Description */
    function setFilterDescription() {
      var tempFilteredClients,
        tempDescriptionResults = $.extend([], descriptionClientInfo);

      if (len) {
        keys = Object.keys(selectedList);
        keysLen = keys.length;
        for (i = 0; i < descriptionLen; i++) {
          for (j = 0; j < keysLen; j++) {
            if (tempDescriptionResults[i].field.indexOf(keys[j]) > -1) {
              isPresent = true;
              break;
            }
          }
          if (!isPresent) {
            tempDescriptionResults.splice(i, 1);
            i--;
            descriptionLen--;
          }
          isPresent = false;
        }
      }

      if (descriptionLen) {
        keys = Object.keys(subProjectList);
        keysLen = keys.length;
        if (len) {
          tempFilteredClients = $.extend([], tempDescriptionResults);
        }
        else {
          tempFilteredClients = $.extend([], filteredClients);
          tempFilteredClients = $.extend(tempFilteredClients, tempDescriptionResults);
        }
        filterDescription(tempFilteredClients);
      }
      finalList = combineResults(finalList, selectedProjects, tempClientsList);
    }

    initializeMarker(finalList);
  }

  /*=====Merge the Final Projects and Clients List=====*/
  function combineResults(finalList, filteredProjects, clientInformation) {
    var toFetchLen, fetchedClient, projectList, toFetchClients = {},
      keys, i,
      projectsLen = filteredProjects.length;

    for (i = 0; i < projectsLen; i++) {
      toFetchClients[filteredProjects[i].clientId] = true;
    }

    keys = Object.keys(toFetchClients);
    toFetchLen = keys.length;

    for (i = 0; i < toFetchLen; i++) {
      fetchedClient = $.extend([], $.grep(clientInformation, function(client) {
        return client._id === keys[i];
      })[0]);

      projectList = $.grep(filteredProjects, function(project) {
        return project.clientId === keys[i];
      });

      fetchedClient.projects = projectList;
      finalList.push(fetchedClient);
      fetchedClient = [];
      projectList = [];
    }

    return finalList;
  }

  /*======Initializing Maps======*/
  initializeMap();

  function initializeMap() {
    var mapOptions = {
      zoom: 3,
      center: new google.maps.LatLng(20, -40),
      mapTypeControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scaleControl: false,
      streetViewControl: false,
      panControl: false,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_BOTTOM
      }
    };

    if ($(window).width() > 1600) {
      mapOptions.zoom = 3;
      mapOptions.center = new google.maps.LatLng(25, -15);
    }

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    infowindow = new google.maps.InfoWindow();
  }

  /*=======Initialize Marker========*/
  function initializeMarker(data) {
    var i, len;

    len = markers.length;
    for (i = 0; i < len; i++) {
      markers[i].setMap(null);
    }
    markers = [];
    len = data.length;
    for (i = 0; i < len; i++) {
      setMarker(data[i]);
    }
  }

  /*=======Place Marker On the Map======*/
  function setMarker(image) {
    var markerIcon, infoContent, marker, field, len, subProjLen, i,
      markerText = '';

    field = (image.field) ? image.field : [];
    len = Object.keys(selectedList).length;
    subProjLen = Object.keys(subProjectList).length;

    if (!image.isClientActive){
      markerIcon = markerImagesPath + markerIconsName.Inactive;
      markerText = ' <small class="inactive"> (Inactive) </small>';
    }
    else {
      for (i = 0; i < fieldNameListLen; i++) {
        setMarkerIcon(practicesNames[i]);
        if (markerIcon) {
          break;
        }
      }
    }

    function setMarkerIcon(fieldName) {
      var conditionLen, conditionList;

      if (modifiedPractices.Horizontal.indexOf(fieldName) > -1) {
        conditionLen = subProjLen;
        conditionList = subProjectList;
      }
      else {
        conditionLen = len;
        conditionList = selectedList;
      }

      if (field.indexOf(fieldName) > -1) {
        if (conditionLen) {
          if (conditionList[fieldName]) {
            markerIcon = markerImagesPath + markerIconsName[fieldName];
          }
        }
        else if (!subProjLen && !len) {
          markerIcon = markerImagesPath + markerIconsName[fieldName];
        }
      }
      conditionList = null;
    }

    if (image.latitude && image.longitude) {
      marker = new google.maps.Marker({
        position: {
          lat: image.latitude,
          lng: image.longitude
        },
        map: map,
        icon: markerIcon
      });

      markers.push(marker);

      marker.addListener('click', function(event) {
        showClientDetails(image);
      });

      marker.addListener('mouseover', function(event) {
        infoContent = '<h1 class="infoHead"> <span>' + image.clientName + '</span> ' + markerText + '</h1>';
        infowindow.setContent(infoContent);
        infowindow.setPosition(event.latLng);
        infowindow.setOptions({
          pixelOffset: new google.maps.Size(0, -50),
          zIndex: 100
        });
        infowindow.open(map);
      });

      marker.addListener('mouseout', function(event) {
        infowindow.close(map);
      });
    }
  }

  $('#logoutBtn').off('click').on('click',function(){
    $.get(urlPrefix + '/logout', function(response){
      window.location.href = urlPrefix + '/';
    });
  });
});