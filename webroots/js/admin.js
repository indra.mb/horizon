(function () {
  "use strict";

  /*======Initialize Angular App======*/
  var myApp = angular.module('mapsAdmin', []);

  /*======Configuration of App======*/
  myApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
  }]);

  /*======Factory to Handle HTTP Calls======*/
  myApp.factory('httpFactory', ['$http', '$q', '$window', '$timeout', function ($http, $q, $window, $timeout) {
    var urlPrefix = $window.location.protocol + '//' + $window.location.hostname + ':' + $window.location.port,
      callCount = 0,
      loader = angular.element(document.getElementById('loader'));

    function postCall() {
      callCount--;
      if (callCount === 0) {
        $timeout(function () {
          loader.removeClass('show');
        }, 1000);
      }
    }
    return {
      urlRequest: function (method, url, requestBody) {
        var deferred = $q.defer(),
          request = {
            method: method,
            url: urlPrefix + url,
            data: requestBody
          };

        callCount++;
        if (callCount === 1) {
          loader.addClass('show');
        }

        $http(request).then(function (response) {
          deferred.resolve(response.data);
          postCall();
        }, function (error) {
          postCall();
          deferred.reject(error.data);
        });

        return deferred.promise;
      }
    };
  }]);

  /*======Factory to Fetch All services======*/
  myApp.factory('serviceFactory', ['httpFactory', function (httpFactory) {
    return {
      getUserRole: function () {
        return httpFactory.urlRequest('GET', '/getUserRole');
      },

      getAllUsers: function () {
        return httpFactory.urlRequest('GET', '/getAllUsers');
      },

      getADUsers: function () {
        return httpFactory.urlRequest('GET', '/getADUsers');
      },

      saveUser: function (method, userData) {
        return httpFactory.urlRequest(method, '/saveUser', userData);
      },

      deleteUser: function (userId) {
        return httpFactory.urlRequest('POST', '/deleteUser', userId);
      },

      getClients: function () {
        return httpFactory.urlRequest('GET', '/getClients');
      },

      getClient: function (clientId) {
        return httpFactory.urlRequest('POST', '/getClient', clientId);
      },

      saveClient: function (method, clientData) {
        return httpFactory.urlRequest(method, '/saveClient', clientData);
      },

      getProjects: function (clientId) {
        return httpFactory.urlRequest('POST', '/getProjects', clientId);
      },

      getProject: function (projectId) {
        return httpFactory.urlRequest('POST', '/getProject', projectId);
      },

      saveProject: function (method, projectData) {
        return httpFactory.urlRequest(method, '/saveProject', projectData);
      },

      getPractices: function () {
        return httpFactory.urlRequest('GET', '/getPractices');
      },

      logout: function () {
        return httpFactory.urlRequest('GET', '/logout');
      }
    }
  }]);

  /*======Application/Main Controller======*/
  myApp.controller('clientsController', ['$scope', '$timeout', '$filter', 'serviceFactory', '$q', '$window', function ($scope, $timeout, $filter, serviceFactory, $q, $window) {
    var bgMap, locationMap, mapsAnimation,
      isNewProject = false,
      wrapperElement = document.getElementsByClassName('content-wrapper')[0];

    $scope.clients = [];
    $scope.adUsers = [];
    $scope.isClientAdded = false;
    $scope.isClientError = false;
    $scope.state = 1;
    $scope.tab = 1;
    $scope.role = 'PMO';
    $scope.isNewUser = false;
    $scope.isSingleAdmin = false;
    $scope.newUser = { editRole: 'PMO' };

    /*======Initialize Application Background Map with Pan Animation======*/
    function initializeBackgroundMap() {
      var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(38.6272222, -90.1977778),
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scaleControl: false,
        streetViewControl: false,
        panControl: false,
        zoomControl: false
      };

      bgMap = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
      panMap();
    }

    function panMap() {
      mapsAnimation = setInterval(function () {
        bgMap.panBy(1, 0.5);
      }, 20);
    }

    /*======Get Current User Role======*/
    function getUserRole() {
      serviceFactory.getUserRole().then(function (data) {
        $scope.role = data;
        if ($scope.role === 'ADMIN') {
          getAllUsers();
          getADUsers();
        }
      });
    }

    /*======Check if Current User is Admin======*/
    function adminCheck(data) {
      var adminCount = $filter('filter')(data, {role : 'ADMIN'}).length;

      $scope.isSingleAdmin = (adminCount > 1) ? false : true;
    }

    /*======Get List of All Users in case of Super Admin Role======*/
    function getAllUsers() {
      serviceFactory.getAllUsers().then(function (data) {
        $scope.users = $filter('orderBy')(data, ['role', 'userName']);
        adminCheck(data);
        angular.forEach(function (obj) {
          obj.isEdit = false;
        });
      });
    }

    function getADUsers() {
      serviceFactory.getADUsers().then(function (data) {
        $scope.adUsers = data;
      });
    }

    /*======Get List of All Clients======*/
    function getClientsList() {
      serviceFactory.getClients().then(function (data) {
        $scope.clients = data;
        $scope.state = 1;
      });
    }

    /*======Get List of Practices======*/
    function getPractices() {
      var deferred = $q.defer();

      if($scope.fields){
        deferred.resolve();
      }
      else{
        serviceFactory.getPractices().then(function (data) {
          $scope.fields = data;
          filterPractices();
          deferred.resolve();
        });
      }

      return deferred.promise;
    }

    /*======Filter List of all Selected Practices======*/
    function filterPractices() {
      var i, len = $scope.fields.length,
        practices = {};

      for(i=0;i<len;i++){
        practices[$scope.fields[i].practice] = true;
      }

      $scope.practices = Object.keys(practices);
    }

    /*======Reset the User section State======*/
    function resetUsers() {
      getAllUsers();
      $scope.newUser = { editRole: 'PMO' };
    }

    /*======Get List of All Projects======*/
    function getProjectsList(clientId) {
      var deferred = $q.defer();

      serviceFactory.getProjects({
        _id: clientId
      }).then(function (data) {
        $scope.projects = data;
        deferred.resolve(data);
      });

      return deferred.promise;
    }

    /*======Method to Initialize Location Select Map======*/
    function initializeLocationMap() {
      var marker, geocoder, autocomplete,
        mapOptions = {
          zoom: 3,
          center: new google.maps.LatLng(40, -100),
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          scaleControl: false,
          streetViewControl: false,
          panControl: false,
          minZoom: 2,
          zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
          }
        },
        markerOption = {
          map: locationMap
        };

      /*======Initialize Location Object and GeoCode Object=======*/
      locationMap = new google.maps.Map(document.getElementById('location-map'), mapOptions);
      geocoder = new google.maps.Geocoder();

      markerOption = {
        map: locationMap
      };

      if ($scope.clientInfo.latitude) {
        markerOption.position = {
          lat: $scope.clientInfo.latitude,
          lng: $scope.clientInfo.longitude
        };
        locationMap.setCenter({lat: $scope.clientInfo.latitude, lng: $scope.clientInfo.longitude});
        locationMap.setZoom(4);
      }

      /*======Initialize Marker======*/
      marker = new google.maps.Marker(markerOption);

      /*======Initialize Maps Location Search Autocomplete======*/
      autocomplete = new google.maps.places.Autocomplete(document.getElementById('searchField'), {
        types: ['geocode']
      });
      autocomplete.bindTo('bounds', locationMap);

      /*======Add Place Changed event to Maps Autocomplete for Rendering Location on Map======*/
      autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();

        if (!place.geometry) {
          return;
        }

        if (place.geometry.viewport) {
          locationMap.fitBounds(place.geometry.viewport);
        }
        else {
          locationMap.setCenter(place.geometry.location);
          locationMap.setZoom(17);
        }
      });

      /*======Event Triggered on Clicking Client Location on Map======*/
      locationMap.addListener('click', function (event) {
        var location, latLng = event.latLng;

        marker.setPosition(event.latLng);

        geocoder.geocode({
          'latLng': latLng
        }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            location = formatResults(results, 'administrative_area_level_1');
            if (!location) {
              location = formatResults(results, 'political');
            }
          }
          $scope.$apply(function () {
            $scope.clientInfo.location = location;
          });
        });

        /*======Method to Apply Latitude and Longitude changes in DOM======*/
        $scope.$apply(function () {
          $scope.clientInfo.latitude = latLng.lat();
          $scope.clientInfo.longitude = latLng.lng();
        });

      });
    }

    /*======Get Formatted Address of the Client from Google API Results======*/
    function formatResults(results, field) {
      var location;

      angular.forEach(results, function (obj) {
        if (!location && (obj.types.indexOf(field) > -1)) {
          location = obj.formatted_address;
        }
      });

      return location || false;
    }

    /*======Method to Fetch All data needed for Clients Page and Set data in page======*/
    function setClientPage(clientId) {
      var clientCall, projectsCall, practicesCall,
        promises = [];

      clientCall = serviceFactory.getClient({
        _id: clientId
      }).then(function (data) {
        $scope.clientInfo = data;
      });
      promises.push(clientCall);

      projectsCall = getProjectsList(clientId);
      promises.push(projectsCall);

      practicesCall = getPractices();
      promises.push(practicesCall);

      return $q.all(promises);
    }

    initializeBackgroundMap();
    getUserRole();
    getClientsList();

    function updateClientStatus(client) {
      var updatedStatus = {
        _id: client._id,
        isClientActive: !client.isClientActive,
        isClientStatusChanged: true
      };

      serviceFactory.saveClient('PUT', updatedStatus).then(function (data) {
        client.isClientActive = !client.isClientActive;
      });
    }

    /*======Toggle Client/Inactive State======*/
    $scope.toggleClient = function (client, evt) {
      evt.stopPropagation();

      serviceFactory.getProjects({
        _id: client._id
      }).then(function (data) {
        var filteredData = $filter('filter')(data, { isProjectActive: true });

        if (filteredData.length > 0) {
          wrapperElement.scrollTop = 0;

          $timeout(function () {
            $scope.isClientError = true;
          }, 1000);

          $timeout(function () {
            $scope.isClientError = false;
          }, 4500);
        }
        else {
          updateClientStatus(client);
        }
      }, function(err){
        console.log(err);
      });
    };

    /*======Toggle Edit User Functionality======*/
    $scope.toggleEdit = function (index, user) {
      $scope.users[index].editName = user.userName;
      $scope.users[index].editRole = user.role;
      $scope.users[index].editEmail = user.email;
      $scope.users[index].isEdit = true;
    };

    /*======Save User======*/
    $scope.saveUser = function (index, user) {
      var method = index ? 'PUT' : 'POST',
        userData = {
          userName: user.editName,
          role: user.editRole,
          email: user.editEmail
        };

      if (index) {
        userData._id = user._id;
      }

      serviceFactory.saveUser(method, userData).then(function () {
        resetUsers();
      });
    };

    /*======Delete a User======*/
    $scope.deleteUser = function (id) {
      serviceFactory.deleteUser({ _id: id }).then(function () {
        resetUsers();
      });
    };

    /*======Open Client Page for the Selected Client======*/
    $scope.openClient = function (clientId) {
      var getClientsData, isClientSet = false;

      $scope.clientInfo = {};
      $scope.projectInfo = {};
      $scope.isClientAdded = true;
      if (clientId) {
        getClientsData = setClientPage(clientId);

        getClientsData.then(function () {
          navigatePage();
        });
      }
      else {
        $scope.isClientAdded = false;
        getPractices();
        navigatePage();
      }

      function navigatePage() {
        $scope.state = 2;
        $scope.clientInfo.field = $scope.clientInfo.field || [];
        $timeout(function () {
          initializeLocationMap();
        });
      }
    };

    /*======Method to Ckeck if the Fields checkbox should be selected in DOM======*/
    $scope.isFieldPresent = function (value) {
      return ($scope.clientInfo.field.indexOf(value) > -1) ? true : false;
    }

    /*======Add/Remove fields based on selection======*/
    $scope.toggleField = function (value) {
      var index = $scope.clientInfo.field.indexOf(value);

      if (index > -1) {
        $scope.clientInfo.field.splice(index, 1);
      }
      else {
        $scope.clientInfo.field.push(value);
      }
    }

    /*======Save a Client======*/
    $scope.saveClient = function () {
      var method = $scope.isClientAdded ? 'PUT' : 'POST';

      $scope.clientInfo.isClientActive = true;
      serviceFactory.saveClient(method, $scope.clientInfo).then(function () {
        getClientsList();
      });
    };

    /*======Toggle a Project as Active or Inactive======*/
    $scope.toggleProject = function (project, evt) {
      var index, filteredClient,
        updatedStatus = {
          _id: project._id,
          isProjectActive: !project.isProjectActive,
          isProjectStatusChanged: true
        };

      evt.stopPropagation();

      if (!$scope.clientInfo.isClientActive && !project.isProjectActive) {
        filteredClient = $filter('filter')($scope.clients, { _id: $scope.clientInfo._id })[0];
        updateClientStatus(filteredClient);
        index = $scope.clients.indexOf(filteredClient);

        $timeout(function () {
          $scope.$apply(function () {
            $scope.clients[index].isClientActive = true;
          });
        });
      }

      serviceFactory.saveProject('PUT', updatedStatus).then(function () {
        project.isProjectActive = !project.isProjectActive;
      });
    };

    /*======Open Details of Project on Selection======*/
    $scope.openProject = function (projectId) {
      isNewProject = false;
      if (projectId) {
        serviceFactory.getProject({
          _id: projectId
        }).then(function (data) {
          $scope.projectInfo = data;
          $scope.state = 3;
          $scope.tab = 2;
        });
      }
      else {
        isNewProject = true;
        $scope.state = 3;
        $scope.tab = 2;
      }
    };

    /*======Save a Project======*/
    $scope.saveProject = function () {
      var method = isNewProject ? 'POST' : 'PUT';

      $scope.projectInfo.isProjectActive = true;
      $scope.projectInfo.clientId = $scope.clientInfo._id;
      serviceFactory.saveProject(method, $scope.projectInfo).then(function () {
        getProjectsList($scope.clientInfo._id);
        $scope.tab = 1;
        isNewProject = false;
      });
    };

    /*======Open Maps Page======*/
    $scope.goToMaps = function () {
      $window.location.href = '/';
    };

    /*======Logout Functionality======*/
    $scope.logout = function () {
      serviceFactory.logout().then(function (response) {
        $window.location.reload();
      });
    };
  }]);

})();