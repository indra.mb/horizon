$(document).ready(function(){
  "use strict";

  var map, mapsAnimation, urlPrefix = location.protocol + '//' + location.hostname + ':' + location.port,
    nameField = $('#name'),
    passwordField = $('#password'),
    invalidLogin = $('#invalidLogin'),
    submitBtn = $('#submit'),
    isAdminLogin = (location.pathname === '/admin') ? true : false;
    
  invalidLogin.addClass('hideError');
  
  function isInputValid(){
    var nameVal = nameField.val(),
      passwordVal = passwordField.val();
      
    if(!nameVal || !passwordVal){
      return false;
    }
    else{
      return {
        username : nameVal,
        password : passwordVal
      };
    }
  }
  
  function checkInput(event){
    if(event.keyCode === 13){
      submitBtn.trigger('click');
    }
  }
  
  nameField.off('keyup').on('keyup', checkInput);
  passwordField.off('keyup').on('keyup', checkInput);
  
  submitBtn.off('click').on('click',function(){
    var inputFields, requestBody = {};
    
    inputFields = isInputValid();
    if(!inputFields){
      invalidLogin.removeClass('hideError');
      return;
    }
    
    invalidLogin.addClass('hideError');

    $.post( urlPrefix + '/userValidation', inputFields, function(response){
      if(response){
        invalidLogin.addClass('hideError');
        if(isAdminLogin){
          window.location.href = urlPrefix + '/admin';
        }
        else{
          window.location.href = urlPrefix + '/';
        }
      }
      else{
        invalidLogin.removeClass('hideError');
      }
    });
  });

  function initializeMap() {
    var mapOptions = {
      zoom: 15,
      center: new google.maps.LatLng(38.6272222, -90.1977778),
      mapTypeControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scaleControl: false,
      streetViewControl: false,
      panControl: false,
      zoomControl: false
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    panMap();
  }

  function panMap(){
    mapsAnimation = setInterval(function(){
      map.panBy(1, 0.5);
    },20);
  }

  initializeMap();
  
});